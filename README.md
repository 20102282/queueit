# Laravel 8 
### Installation

#### -clone this repo and then open this project into your code editor (vs code recommended) 
```
#### -rename .env.example to .env

#### Konfigurasi Mysql
buat database baru bernama laravel
sesuaikan user dan password mysql kalian di file .env

#### -run composer
```
composer install
```
#### -run key generate
```
php artisan key:generate
```
#### -set your database in file .env and in your phpmysql or etc

#### -run migrate
```
php artisan migrate
```

#### -run 
```
php artisan serve
```
