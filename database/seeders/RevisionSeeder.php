<?php
// database/seeders/RevisionSeeder.php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Revision;

class RevisionSeeder extends Seeder
{
    public function run()
    {
        Revision::factory(10)->create(); // Ganti angka 10 sesuai dengan jumlah data yang Anda inginkan
    }
}
