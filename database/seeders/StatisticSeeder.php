<?php
// database/seeders/StatisticSeeder.php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Statistic;

class StatisticSeeder extends Seeder
{
    public function run()
    {
        Statistic::factory(1)->create();
    }
}
