<?php
// database/seeders/CardSeeder.php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Card;

class CardSeeder extends Seeder
{
    public function run()
    {
        Card::factory(10)->create(); // Ganti angka 10 sesuai dengan jumlah data yang Anda inginkan
    }
}
