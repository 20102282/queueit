<?php
// database/seeders/LogSeeder.php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Log;

class LogSeeder extends Seeder
{
    public function run()
    {
        Log::factory(5)->create(); // Ganti angka 20 sesuai dengan jumlah data yang Anda inginkan
    }
}
