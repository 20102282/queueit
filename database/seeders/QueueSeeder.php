<?php

namespace Database\Seeders;
use App\Models\Queue;
use Illuminate\Database\Seeder;

class QueueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Queue::factory(10)->create(); // Ganti angka 10 sesuai dengan jumlah data yang Anda inginkan

    }
}
