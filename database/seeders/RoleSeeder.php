<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['name' => 'data', 'description' => 'Role untuk menerima informasi klien dan memasukkan data'],
            ['name' => 'desain', 'description' => 'Role untuk mendesain kartu sesuai inputan dari role data'],
            ['name' => 'tinjau', 'description' => 'Role untuk meninjau kartu dan memberikan persetujuan'],
            ['name' => 'print', 'description' => 'Role untuk mencetak kartu yang sudah dalam tahap final'],
        ];
        DB::table('roles')->insert($roles);
    }
}
