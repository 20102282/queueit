<?php

// database/seeders/ReviewSeeder.php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Review;

class ReviewSeeder extends Seeder
{
    public function run()
    {
        Review::factory(10)->create(); // Ganti angka 10 sesuai dengan jumlah data yang Anda inginkan
    }
}
