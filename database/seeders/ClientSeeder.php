<?php
// database/seeders/ClientSeeder.php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Client;

class ClientSeeder extends Seeder
{
    public function run()
    {
        Client::factory(10)->create(); // Ganti angka 10 sesuai dengan jumlah data yang Anda inginkan
    }
}
