<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class, // Panggil class seeder yang dibuat
            ClientSeeder::class,
            CardSeeder::class,
            QueueSeeder::class,
            ReviewSeeder::class,
            RevisionSeeder::class,
            StatisticSeeder::class,
            LogSeeder::class,
            // Tambahkan seeder lainnya jika ada
        ]);
    }
}
