<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            //name
            $table->string('name');
            //address
            $table->string('address');
            //instansi
            $table->string('instansi');
            //email
            $table->string('email');
            //phone
            $table->string('phone');
            //date
            $table->date('date');
            //notes
            $table->text('notes')->nullable();
            //boolean activer user or not
            $table->boolean('is_active')->default(true);

             // Tambahkan kolom untuk gambar
             $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
