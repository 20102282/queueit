<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('card_id');
            $table->date('review_date');
            $table->text('notes')->nullable();
           // $table->enum('status', ['approved', 'returned']);
           //changed to boolean
            $table->boolean('status')->default(0)->comment('0:approved, 1:returned');
            $table->timestamps();

            $table->foreign('card_id')->references('id')->on('cards');
        });
    }

    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}


