<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id');
            $table->string('design_image')->nullable();
            $table->boolean('status')->default(0)->comment('0:in_process, 1:reviewed, 2:approved, 3:printed');
            //$table->enum('status', ['in_process', 'reviewed', 'approved', 'printed']);
            // date
            $table->date('date');
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients');
        });
    }

    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
