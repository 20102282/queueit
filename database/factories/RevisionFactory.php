<?php
// database/factories/RevisionFactory.php

namespace Database\Factories;

use App\Models\Revision;
use Illuminate\Database\Eloquent\Factories\Factory;

class RevisionFactory extends Factory
{
    protected $model = Revision::class;

    public function definition()
    {
        return [
            'card_id' => rand(1, 10), // Ganti dengan range ID sesuai data di tabel cards
            'user_id' => rand(1, 10), // Ganti dengan range ID sesuai data di tabel users
            'revision_date' => $this->faker->date,
            'notes' => $this->faker->paragraph,
        ];
    }
}
