<?php
// database/factories/QueueFactory.php

namespace Database\Factories;

use App\Models\Queue;
use Illuminate\Database\Eloquent\Factories\Factory;

class QueueFactory extends Factory
{
    protected $model = Queue::class;

    public function definition()
    {
        return [
            'user_id' => rand(1, 10), // Ganti dengan range ID sesuai data di tabel users
            'card_id' => rand(1, 10), // Ganti dengan range ID sesuai data di tabel cards
            'progress' => $this->faker->numberBetween(0, 100),
            'date' => $this->faker->date,
            'notes' => $this->faker->paragraph,
        ];
    }
}
