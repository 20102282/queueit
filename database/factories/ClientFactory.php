<?php
// database/factories/ClientFactory.php

namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    protected $model = Client::class;

    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'address' => $this->faker->address,
            'instansi' => $this->faker->company,
            'email' => $this->faker->unique()->safeEmail,
            'phone' => $this->faker->phoneNumber,
            'date' => $this->faker->date,
            'notes' => $this->faker->paragraph,
            'is_active' => $this->faker->boolean,
            'image' => null, // Atur null atau sesuai dengan path gambar default
        ];
    }
}
