<?php
// database/factories/StatisticFactory.php

namespace Database\Factories;

use App\Models\Statistic;
use Illuminate\Database\Eloquent\Factories\Factory;

class StatisticFactory extends Factory
{
    protected $model = Statistic::class;

    public function definition()
    {
        return [
            'total_cards_created' => $this->faker->numberBetween(0, 100),
            'total_cards_approved' => $this->faker->numberBetween(0, 100),
            'total_cards_revised' => $this->faker->numberBetween(0, 100),
            'average_creation_time' => $this->faker->randomFloat(2, 0, 100),
        ];
    }
}
