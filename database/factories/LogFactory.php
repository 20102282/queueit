<?php
// database/factories/LogFactory.php

namespace Database\Factories;

use App\Models\Log;
use Illuminate\Database\Eloquent\Factories\Factory;

class LogFactory extends Factory
{
    protected $model = Log::class;

    public function definition()
    {
        return [
            'user_id' => rand(1, 10), // Ganti dengan range ID sesuai data di tabel users
            'activity' => $this->faker->randomElement(['login', 'logout', 'send_progress', 'review_card']),
            'activity_datetime' => $this->faker->dateTime,
        ];
    }
}
