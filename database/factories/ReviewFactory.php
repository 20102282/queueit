<?php
// database/factories/ReviewFactory.php

namespace Database\Factories;

use App\Models\Review;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReviewFactory extends Factory
{
    protected $model = Review::class;

    public function definition()
    {
        return [
            'card_id' => rand(1, 10), // Ganti dengan range ID sesuai data di tabel cards
            'review_date' => $this->faker->date,
            'notes' => $this->faker->paragraph,
            'status' => $this->faker->boolean, // Ganti sesuai dengan kebutuhan
        ];
    }
}
