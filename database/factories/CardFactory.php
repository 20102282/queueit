<?php
// database/factories/CardFactory.php

namespace Database\Factories;

use App\Models\Card;
use Illuminate\Database\Eloquent\Factories\Factory;

class CardFactory extends Factory
{
    protected $model = Card::class;

    public function definition()
    {
        return [
            'client_id' => rand(1, 10), // Ganti dengan range ID sesuai data di tabel clients
            'design_image' => null,
            'status' => $this->faker->randomElement([0, 1, 2, 3]),
            'date' => $this->faker->date,
        ];
    }
}
