<?php
// app/Models/Statistic.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    use HasFactory;

    protected $fillable = [
        'total_cards_created',
        'total_cards_approved',
        'total_cards_revised',
        'average_creation_time',
    ];
}
