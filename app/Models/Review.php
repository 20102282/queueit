<?php
// app/Models/Review.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $fillable = [
        'card_id',
        'review_date',
        'notes',
        'status',
    ];

    // Relasi dengan model Card
    public function card()
    {
        return $this->belongsTo(Card::class);
    }
}
