<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'design_image',
        'status',
        'date',
    ];
    public function user()
    {
        return $this->belongsTo(Client::class);
    }
}
