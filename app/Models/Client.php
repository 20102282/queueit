<?php

// app/Models/Client.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    // Tentukan atribut-atribut yang dapat diisi (fillable)
    protected $fillable = [
        'name',
        'address',
        'instansi',
        'email',
        'phone',
        'date',
        'notes',
        'is_active',
        'image',
    ];
}
