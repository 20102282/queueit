<?php
// app/Models/Revision.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Revision extends Model
{
    use HasFactory;

    protected $fillable = [
        'card_id',
        'user_id',
        'revision_date',
        'notes',
    ];

    // Relasi dengan model Card
    public function card()
    {
        return $this->belongsTo(Card::class);
    }

    // Relasi dengan model User
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
